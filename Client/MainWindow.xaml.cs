﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private byte[] buffer;
        string lamp = "0";

        public MainWindow()
        {
            InitializeComponent();

            var endPoint = new IPEndPoint(IPAddress.Loopback, 3000);
            _clientSocket.BeginConnect(endPoint, ConnectCallback, null);

            
        }


        // btnLamp area check lamp on/off
        private void btnLamp_Click(object sender, RoutedEventArgs e)
        {
            if(lamp == "0")
            {
                btnLamp.Content = "Lamp Off";
                btnLamp.Foreground = Brushes.OrangeRed;

                byte[] buffer = Encoding.ASCII.GetBytes(lamp);
                _clientSocket.Send(buffer);
                lamp = "1";

            }
            else
            {

                btnLamp.Content = "Lamp On";
                btnLamp.Foreground = Brushes.DarkSeaGreen;
                byte[] buffer = Encoding.ASCII.GetBytes(lamp);
                _clientSocket.Send(buffer);
                lamp = "0";

            }

            // Send new status to server

            // Bu kısma bak value sorunu olabilir atlama.


        }

        // Send request for create random number .
        private void btnText_Click(object sender, RoutedEventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("rnd");
            _clientSocket.Send(buffer);
        }

        
        private void ConnectCallback(IAsyncResult AR)
        {
            try
            {
                _clientSocket.EndConnect(AR);

                buffer = new byte[_clientSocket.ReceiveBufferSize];
                _clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
                txtPort.Dispatcher.Invoke(() =>
                {
                    txtPort.Text = "Connected To Server";
                    txtPort.Foreground = Brushes.DarkSeaGreen;
                });
               
            }
            catch
            {
                txtPort.Dispatcher.Invoke(() =>
                {
                    txtPort.Text = "Connect Failed To Server";
                    txtPort.Foreground = Brushes.RosyBrown;
                });
               
            }
            

        }

        private void SendCallback(IAsyncResult AR)
        {
                _clientSocket.EndSend(AR);
         
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            int received = _clientSocket.EndReceive(AR);
            
            if (received == 0)
            {
                return;
            }

            try
            {
                
                string message = Encoding.ASCII.GetString(buffer);
                StringBuilder msg = new StringBuilder();
                foreach (char i in message)
                {
                    if (i.Equals('\0'))
                    {
                        break;
                    }
                    else
                    {
                        msg.Append(Convert.ToChar(i).ToString());
                    }
                }
                if(msg.ToString() == "1")
                {
                    btnLamp.Dispatcher.Invoke(() =>
                    {
                        btnLamp.Content = "Lamp On";
                        btnLamp.Foreground = Brushes.DarkSeaGreen;
                        
                    });
                }
                else if(msg.ToString() == "0") {
                    btnLamp.Dispatcher.Invoke(() =>
                    {
                        btnLamp.Content = "Lamp Off";
                        btnLamp.Foreground = Brushes.OrangeRed;
                    });
                }
                else
                {   
                    btnText.Dispatcher.Invoke(() =>
                    {
                        btnText.Content = msg.ToString();
                        
                    });
                }
                
            }
            catch
            {


            }

            // Start receiving data again.
            // Bir daha bufferı boşaltmayı unutma !!!
            Array.Clear(buffer, 0, buffer.Length);
            _clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
            
        }

    }
}
